package handler

import (
	"net/http"
	"usersapi/internal/application/context"
	"usersapi/internal/application/writer"
	"usersapi/internal/domain/user"
	"usersapi/internal/infra/repository"

	"github.com/labstack/echo/v4"
)

func New(c echo.Context) error {
	u := &user.User{}

	if err := c.Bind(u); err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	if invalidBody(u) {
		return c.String(http.StatusBadRequest, "Invalid request body")
	}

	ctx := c.(*context.Context)

	r := repository.NewRepository(ctx.GetDBConnection())

	e := user.NewUserEntity(r)

	created, err := e.NewUser(u)

	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	j, _ := writer.ToJSON(created)

	return c.String(http.StatusCreated, j)
}

func invalidBody(u *user.User) bool {
	return u.Name == "" || u.Email == ""
}
