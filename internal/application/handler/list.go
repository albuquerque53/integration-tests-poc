package handler

import (
	"net/http"
	"usersapi/internal/application/context"
	"usersapi/internal/application/writer"
	"usersapi/internal/domain/user"
	"usersapi/internal/infra/repository"

	"github.com/labstack/echo/v4"
)

func ListUsers(c echo.Context) error {
	ctx := c.(*context.Context)

	r := repository.NewRepository(ctx.GetDBConnection())

	e := user.NewUserEntity(r)

	u, err := e.ListUsers()

	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	j, _ := writer.ToJSON(u)

	return c.String(http.StatusOK, j)
}
