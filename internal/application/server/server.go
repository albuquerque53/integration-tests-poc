package server

import (
	"usersapi/internal/application/context"
	"usersapi/internal/application/handler"

	"github.com/labstack/echo/v4"
)

func SetupServer(ctx *context.Context) *echo.Echo {
	e := echo.New()

	e.GET("/list", handler.ListUsers)
	e.POST("/new", handler.New)

	e.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			ctx.Context = c
			return next(ctx)
		}
	})

	return e
}
