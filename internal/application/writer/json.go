package writer

import "encoding/json"

func ToJSON(value any) (string, error) {
	j, err := json.Marshal(value)

	if err != nil {
		return "", err
	}

	return string(j), err
}
