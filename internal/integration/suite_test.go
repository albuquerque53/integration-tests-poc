package integration

import (
	"database/sql"
	"fmt"
	"net/http/httptest"
	"strings"
	"testing"
	"usersapi/internal/application/context"
	"usersapi/internal/application/server"
	"usersapi/internal/infra/db"

	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

type Suite struct {
	suite.Suite
	DBConn                  *sql.DB
	Srv                     *httptest.Server
	Migration               *db.Migration
	MigrationLocationFolder string
}

func (s *Suite) SetupSuite() {
	var err error

	s.DBConn = db.ConectToDatabase()

	_, err = s.DBConn.Exec("set global sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';")
	require.NoError(s.T(), err)

	_, err = s.DBConn.Exec("set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';")
	require.NoError(s.T(), err)

	s.Migration, err = db.RunMigration(s.DBConn, "../infra/db/migrations")
	require.NoError(s.T(), err)
}

func (s *Suite) TearDownSuite() {
	s.DBConn.Close()
}

func TestSuite(t *testing.T) {
	suite.Run(t, new(Suite))
}

func (s *Suite) SetupTest() {
	err := s.Migration.Up()
	require.NoError(s.T(), err)

	ctx := &context.Context{}
	ctx.SetDBConnection(s.DBConn)

	app := server.SetupServer(ctx)
	s.Srv = httptest.NewServer(app)
}

func (s *Suite) TearDownTest() {
	err := s.Migration.Down()
	require.NoError(s.T(), err)
	s.Srv.Close()
}

func seeInDatabase(s *Suite, table string, search map[string]string) {
	var keys []string
	var values []any
	q := fmt.Sprintf("SELECT COUNT(id) AS total FROM %s WHERE ", table)

	for key, value := range search {
		keys = append(keys, fmt.Sprintf("%s=?", key))
		values = append(values, any(value))
	}

	var row struct {
		Total int `json:"total"`
	}

	err := s.DBConn.QueryRow(q+strings.Join(keys, " and "), values...).Scan(&row.Total)

	s.NoError(err, "Expected no error during seeInDatabase() scan")

	if row.Total > 0 {
		return
	}

	s.Fail(fmt.Sprintf("Could not found registry in %s for %v", table, search))
}
